import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Created by wust on 2019/6/13.
 */
public class TestMain {
   public static void main(String[] args){
       String pattern = "a/b/[a-zA-Z0-9]";

       String content = "a/b/&";

       boolean isMatch = Pattern.matches(pattern, content);
       System.out.println("字符串中是否包含了 'runoob' 子字符串? " + isMatch);



       File file1 = new File("D:\\本班全部同学.txt");
       File file2 = new File("D:\\本班接龙同学.txt");

       Set<String> sets = new HashSet<>();
       BufferedReader br1 = null;

       BufferedReader br2 = null;
       try{
           br1 = new BufferedReader(new FileReader(file2));
           String s1 = null;
           while((s1 = br1.readLine())!=null){
               String description = Pattern.compile("[\\d\\.\\、]").matcher(s1).replaceAll("");
               System.out.println("=="+description);
               sets.add(description.replace(" ",""));
           }


           System.out.println("以下同学没有签到：");
           br2 = new BufferedReader(new FileReader(file1));
           String s2 = null;
           while((s2 = br2.readLine())!=null){
               boolean c = false;
               Iterator<String> iterator = sets.iterator();
               while (iterator.hasNext()){
                   String key = iterator.next();
                   if(key.contains(s2)){
                       c = true;
                       break;
                   }
               }

              if(!c){
                   System.err.println(s2);
               }
           }



       }catch(Exception e){
           e.printStackTrace();
       }finally {
           if(br1 != null){
               try {
                   br1.close();
               } catch (IOException e) {
                   e.printStackTrace();
               }
           }

           if(br2 != null){
               try {
                   br2.close();
               } catch (IOException e) {
                   e.printStackTrace();
               }
           }
       }


       String st1 = "%s_nihao%s_666";
       System.out.println(st1.replaceAll("%s_","*"));
   }
}
