package com.sc.admin.core.dao;


import com.sc.common.mapper.IBaseMapper;
import com.sc.common.entity.admin.account.resource.SysAccountResource;

/**
 * @author: wust
 * @date: 2020-03-26 09:45:35
 * @description:
 */
public interface SysAccountResourceMapper extends IBaseMapper<SysAccountResource> {
}