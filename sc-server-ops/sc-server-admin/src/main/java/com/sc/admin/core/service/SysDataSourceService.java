package com.sc.admin.core.service;


import com.sc.common.entity.admin.datasource.SysDataSource;
import com.sc.common.service.BaseService;


/**
 * Created by wust on 2019/6/17.
 */
public interface SysDataSourceService extends BaseService<SysDataSource> {

}
