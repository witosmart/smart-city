/**
 * Created by wust on 2020-03-30 10:44:32
 * Copyright © 2020 wust. All rights reserved.
 */
package com.sc.admin.core.cache;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.fastjson.JSONObject;
import com.sc.admin.core.dao.SysLookupMapper;
import com.sc.common.entity.admin.lookup.SysLookup;
import com.sc.common.enums.RedisKeyEnum;
import com.sc.common.util.cache.SpringRedisTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.entity.Example;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author: wust
 * @date: Created in 2020-03-30 10:44:32
 * @description: 数据字典缓存，不走mq通知
 *
 */
@Component
public class RedisCacheLookupBo {
    @Autowired
    private SpringRedisTools springRedisTools;

    @Autowired
    private SysLookupMapper sysLookupMapper;

    private volatile boolean deleteFlag = false;


    public void init() {
        reset();
    }

    public void reset() {
        Set<String> keys1 = springRedisTools.keys("*REDIS_KEY_HASH_GROUP_LOOKUP_BY_ROOT_CODE_AND_NAME_AND_LANG");
        if(keys1 != null && keys1.size() > 0){
            springRedisTools.deleteByKey(keys1);
        }

        Set<String> keys2 = springRedisTools.keys("*REDIS_KEY_HASH_GROUP_LOOKUP_BY_CODE_AND_LANG");
        if(keys2 != null && keys2.size() > 0){
            springRedisTools.deleteByKey(keys2);
        }

        Set<String> keys3 = springRedisTools.keys("*REDIS_KEY_HASH_GROUP_LOOKUP_BY_PID_AND_LANG");
        if(keys3 != null && keys3.size() > 0){
            springRedisTools.deleteByKey(keys3);
        }

        Example example = new Example(SysLookup.class);
        example.setOrderByClause("sort ASC");
        List<SysLookup> lookupList = sysLookupMapper.selectByExample(example);
        if(CollectionUtil.isNotEmpty(lookupList)){
            for (SysLookup sysLookup : lookupList) {
                add(sysLookup);
            }
        }
    }


    public void add(Object obj) {
        if(!deleteFlag){
            deleteAll();
        }
        if(obj == null){
            return;
        }
        SysLookup sysLookup = null;

        if(obj instanceof SysLookup){
            sysLookup = (SysLookup)obj;
        }else if(obj instanceof Map){
            sysLookup = JSONObject.parseObject(JSONObject.toJSONString(obj),SysLookup.class);
        }


        if(sysLookup == null){
            return;
        }

        cacheByRootCodeAndNameAndLang(sysLookup);

        cacheByCodeAndLang(sysLookup);

        cacheByPidAndLang(sysLookup);
    }


    public void batchAdd(List<Object> list) {
        if(!deleteFlag){
            deleteAll();
        }
        if(CollectionUtil.isNotEmpty(list)){
            for (Object o : list) {
                add(o);
            }
        }
    }


    public void updateByPrimaryKey(Object primaryKey) {

    }


    public void batchUpdate(List<Object> list) {

    }


    public void deleteByPrimaryKey(Object primaryKey) {

    }


    public void batchDelete(List<Object> primaryKeys) {

    }



    private void cacheByRootCodeAndNameAndLang(SysLookup lookup){
        String rootCode = lookup.getRootCode();
        String name = lookup.getName();
        String lang = lookup.getLan();

        String key1 = String.format(RedisKeyEnum.REDIS_KEY_HASH_GROUP_LOOKUP_BY_ROOT_CODE_AND_NAME_AND_LANG.getStringValue(),rootCode,name,lang);
        if(springRedisTools.hasKey(key1)){
            springRedisTools.deleteByKey(key1);
        }

        Map mapValue1 = BeanUtil.beanToMap(lookup);
        springRedisTools.addMap(key1,mapValue1);
    }


    private void cacheByCodeAndLang(SysLookup lookup){
        String code = lookup.getCode();
        String lang = lookup.getLan();

        String key2 = String.format(RedisKeyEnum.REDIS_KEY_HASH_GROUP_LOOKUP_BY_CODE_AND_LANG.getStringValue(),code,lang);
        if(springRedisTools.hasKey(key2)){
            springRedisTools.deleteByKey(key2);
        }
        Map mapValue2 = BeanUtil.beanToMap(lookup);
        springRedisTools.addMap(key2,mapValue2);
    }


    private void cacheByPidAndLang(SysLookup lookup){
        String code = lookup.getCode();
        String lang = lookup.getLan();

        Example example1 = new Example(SysLookup.class);
        Example.Criteria criteria = example1.createCriteria();
        criteria.andEqualTo("parentCode",code);
        example1.setOrderByClause("sort ASC");
        List<SysLookup> lookupListChildren = sysLookupMapper.selectByExample(example1);
        if(CollectionUtil.isNotEmpty(lookupListChildren)){
            String key3 = String.format(RedisKeyEnum.REDIS_KEY_HASH_GROUP_LOOKUP_BY_PID_AND_LANG.getStringValue(),code,lang);
            if(springRedisTools.hasKey(key3)){
                springRedisTools.deleteByKey(key3);
            }

            Map mapValue3 = new HashMap();
            for (SysLookup lookupListChild : lookupListChildren) {
                mapValue3.put(lookupListChild.getCode(),lookupListChild);
            }
            springRedisTools.addMap(key3,mapValue3);
        }
    }

    private void deleteAll(){
        Set<String> keys1 = springRedisTools.keys("*REDIS_KEY_HASH_GROUP_LOOKUP_BY_ROOT_CODE_AND_NAME_AND_LANG");
        if(keys1 != null && keys1.size() > 0){
            springRedisTools.deleteByKey(keys1);
        }

        Set<String> keys2 = springRedisTools.keys("*REDIS_KEY_HASH_GROUP_LOOKUP_BY_CODE_AND_LANG");
        if(keys2 != null && keys2.size() > 0){
            springRedisTools.deleteByKey(keys2);
        }

        Set<String> keys3 = springRedisTools.keys("*REDIS_KEY_HASH_GROUP_LOOKUP_BY_PID_AND_LANG");
        if(keys3 != null && keys3.size() > 0){
            springRedisTools.deleteByKey(keys3);
        }

        deleteFlag = true;
    }
}
