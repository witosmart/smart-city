package com.sc.admin.core.service;


import com.sc.common.entity.admin.role.SysRole;
import com.sc.common.service.BaseService;

/**
 * Created by wust on 2019/5/27.
 */
public interface SysRoleService extends BaseService<SysRole> {

}
