/**
 * Created by wust on 2020-03-31 09:13:01
 * Copyright © 2020 wust. All rights reserved.
 */
package com.sc.admin.core.service.impl;

import cn.hutool.crypto.SecureUtil;
import com.sc.admin.core.dao.SysAccountMapper;
import com.sc.common.entity.admin.account.SysAccount;
import com.sc.common.enums.ApplicationEnum;
import com.sc.common.service.InitializtionService;
import com.sc.common.util.RC4;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.Date;

/**
 * @author: wust
 * @date: Created in 2020-03-31 09:13:01
 * @description: 系统启动，判断账号表是否有超级管理员账号，没有则初始化一个
 *
 */
@Order(2)
@Service
public class InitData4AccountServiceImpl implements InitializtionService {
    @Autowired
    private SysAccountMapper sysAccountMapper;

    @Transactional(rollbackFor=Exception.class)
    @Override
    public void init() {
        SysAccount accountSearch = new SysAccount();
        accountSearch.setType("A101701");
        accountSearch.setAccountCode(ApplicationEnum.SYSTEM_ADMIN_ACCOUNT.getStringValue());
        SysAccount account = sysAccountMapper.selectOne(accountSearch);
        if(account == null){
            String passwordRC4 = RC4.encry_RC4_string(SecureUtil.md5("admin123").toUpperCase(), ApplicationEnum.LOGIN_RC4_KEY.getStringValue());
            account = new SysAccount();
            account.setAccountCode(ApplicationEnum.SYSTEM_ADMIN_ACCOUNT.getStringValue());
            account.setAccountName("系统超级管理员");
            account.setType("A101701"); // 管理员
            account.setPassword(passwordRC4);
            account.setCreateTime(new Date());
            account.setIsDeleted(0);
            this.sysAccountMapper.insert(account);
        }
    }
}
