/**
 * Created by wust on 2019-11-01 10:14:28
 * Copyright © 2019 wust. All rights reserved.
 */
package com.sc.admin.core.api.open.customer;



import com.sc.common.annotations.OpenApi;
import com.sc.common.util.cache.SpringRedisTools;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: wust
 * @date: Created in 2019-11-01 10:14:28
 * @description: app客户登录：账号密码登录
 *
 */
@Api(tags = {"开放接口~app客户账号密码登陆"})
@OpenApi
@RequestMapping("/api/open/v1/AppCustomerLoginByAccountOpenApi")
@RestController
public class AppCustomerLoginByAccountOpenApi {
    @Autowired
    private SpringRedisTools springRedisTools;

}
