package com.sc.admin.core.xml.factory;



import com.sc.admin.core.xml.XMLAbstractResolver;
import com.sc.admin.core.xml.XMLDefinitionFactory;
import com.sc.admin.core.xml.resolver.XMLPermissionResolver;

public class XMLPermissionFactory implements XMLDefinitionFactory {

    @Override
    public XMLAbstractResolver createXMLResolver() {
        XMLAbstractResolver xmlAbstractResolver = new XMLPermissionResolver();
        return xmlAbstractResolver;
    }
}
