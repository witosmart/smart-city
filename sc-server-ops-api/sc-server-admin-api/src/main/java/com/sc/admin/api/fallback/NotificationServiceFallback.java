package com.sc.admin.api.fallback;

import com.sc.admin.api.NotificationService;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.entity.admin.notification.SysNotification;
import org.springframework.stereotype.Component;

@Component
public class NotificationServiceFallback implements NotificationService {
    @Override
    public WebResponseDto create(String ctx, SysNotification entity) {
        WebResponseDto webResponseDto = new WebResponseDto();
        webResponseDto.setFlag(WebResponseDto.INFO_ERROR);
        webResponseDto.setMessage("服务异常");
        return webResponseDto;
    }
}
