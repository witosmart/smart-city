package com.sc.demo2.api;


import com.sc.common.dto.WebResponseDto;
import com.sc.demo2.api.fallback.TestServiceFallback;
import com.sc.demo2.entity.test.Test2;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "demo2-server",fallback = TestServiceFallback.class)
public interface TestService {
    String API_PREFIX = "/api/feign/v1/TestFeignApi";
    String SELECT = API_PREFIX + "/select";

    @RequestMapping(value = SELECT, method = RequestMethod.POST, consumes = "application/json")
    WebResponseDto select(@RequestHeader(name = "x-ctx", required = true) String ctx, @RequestBody Test2 search);
}
