package com.sc.generatorcode.utils;

import freemarker.template.Template;
import freemarker.template.TemplateException;

import java.io.*;

/**
 * @author ：wust
 * Date   2018/4/19
 */
public class FileUtil {

    /**
     * @param type     使用模板类型
     * @param data     填充数据
     * @param fileDir  输出文件目录
     * @param fileName 输出文件名
     * @throws IOException
     * @throws TemplateException
     */
    public static void generateFile(int type, Object data, String fileDir,String fileName) throws IOException, TemplateException {
        File dir = new File(fileDir);
        if (!dir.exists()) {
           dir.mkdirs();
        }

        String filePath = fileDir + File.separator + fileName;
        File file = new File(filePath);
        if(file.exists()){
            System.err.println("该文件"+fileName+"已经存在");
            return;
        }

        Template tpl = getTemplate(type); // 获取模板文件
        // 填充数据
        StringWriter writer = new StringWriter();
        tpl.process(data, writer);
        writer.flush();
        // 写入文件
        FileOutputStream fos = new FileOutputStream(filePath);
        OutputStreamWriter osw = new OutputStreamWriter(fos, "UTF-8");
        BufferedWriter bw = new BufferedWriter(osw, 1024);
        tpl.process(data, bw);
        fos.close();
    }

    /**
     * 获取模板
     *
     * @param type 模板类型
     * @return
     * @throws IOException
     */
    private static Template getTemplate(int type) throws IOException {
        switch (type) {
            case FreemarkerConfigUtils.TYPE_ENTITY:
                return FreemarkerConfigUtils.getInstance().getTemplate("Entity.ftl");
            case FreemarkerConfigUtils.TYPE_ENTITY_LIST:
                return FreemarkerConfigUtils.getInstance().getTemplate("EntityList.ftl");
            case FreemarkerConfigUtils.TYPE_ENTITY_SEARCH:
                return FreemarkerConfigUtils.getInstance().getTemplate("EntitySearch.ftl");
            case FreemarkerConfigUtils.TYPE_DAO:
                return FreemarkerConfigUtils.getInstance().getTemplate("Mapper.ftl");
            case FreemarkerConfigUtils.TYPE_SERVICE:
                return FreemarkerConfigUtils.getInstance().getTemplate("ServiceImpl.ftl");
            case FreemarkerConfigUtils.TYPE_CONTROLLER:
                return FreemarkerConfigUtils.getInstance().getTemplate("Controller.ftl");
            case FreemarkerConfigUtils.TYPE_MAPPER:
                return FreemarkerConfigUtils.getInstance().getTemplate("MapperXml.ftl");
            case FreemarkerConfigUtils.TYPE_INTERFACE:
                return FreemarkerConfigUtils.getInstance().getTemplate("Service.ftl");
            case FreemarkerConfigUtils.TYPE_VUE_LIST:
                return FreemarkerConfigUtils.getInstance().getTemplate("VueList.ftl");
            case FreemarkerConfigUtils.TYPE_VUE_CREATE:
                return FreemarkerConfigUtils.getInstance().getTemplate("VueCreate.ftl");
            case FreemarkerConfigUtils.TYPE_VUE_UPDATE:
                return FreemarkerConfigUtils.getInstance().getTemplate("VueUpdate.ftl");
            case FreemarkerConfigUtils.TYPE_VUE_SEARCH_BAR:
                return FreemarkerConfigUtils.getInstance().getTemplate("VueSearchBar.ftl");
            case FreemarkerConfigUtils.TYPE_VUE_IMPORT:
                return FreemarkerConfigUtils.getInstance().getTemplate("VueImport.ftl");
            case FreemarkerConfigUtils.TYPE_IMPORT_EXCEL_XML:
                return FreemarkerConfigUtils.getInstance().getTemplate("ImportExcelXml.ftl");
            case FreemarkerConfigUtils.TYPE_EXPORT_EXCEL_XML:
                return FreemarkerConfigUtils.getInstance().getTemplate("ExportExcelXml.ftl");
            case FreemarkerConfigUtils.TYPE_ENTITY_IMPORT:
                return FreemarkerConfigUtils.getInstance().getTemplate("EntityImport.ftl");
            case FreemarkerConfigUtils.TYPE_IMPORT_SERVICE:
                return FreemarkerConfigUtils.getInstance().getTemplate("ImportService.ftl");
            case FreemarkerConfigUtils.TYPE_IMPORT_SERVICE_IMPL:
                return FreemarkerConfigUtils.getInstance().getTemplate("ImportServiceImpl.ftl");
            default:
                return null;
        }
    }

    public static String getBasicProjectPath() {
        String path = new File(FileUtil.class.getClassLoader().getResource("").getFile()).getPath() + File.separator;
        StringBuilder sb = new StringBuilder();
        sb.append(path.substring(0, path.indexOf("target"))).append("src").append(File.separator).append("main").append(File.separator);
        return sb.toString();
    }

    /**
     * 获取源码路径
     *
     * @return
     */
    public static String getSourcePath() {
        StringBuilder sb = new StringBuilder();
        sb.append(getBasicProjectPath()).append("java").append(File.separator);
        return sb.toString();
    }

    /**
     * 获取资源文件路径
     *
     * @return
     */
    public static String getResourcePath() {
        StringBuilder sb = new StringBuilder();
        sb.append(getBasicProjectPath()).append("resources").append(File.separator);
        return sb.toString();
    }


    public static void mkdirs(String path) throws IOException {
        if (path != null && path.replaceAll(" ","").length() > 0 && !path.equalsIgnoreCase("null")) {
            File file = new File(path);
            if(!file.exists() || !file.isDirectory()) {
                file.mkdirs();
            }
        }
    }
}
