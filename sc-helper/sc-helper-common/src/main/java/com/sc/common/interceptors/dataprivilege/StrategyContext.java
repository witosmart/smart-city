package com.sc.common.interceptors.dataprivilege;

import com.sc.common.enums.DataDictionaryEnum;
import com.sc.common.context.DefaultBusinessContext;
import com.sc.common.exception.BusinessException;
import org.apache.ibatis.executor.statement.BaseStatementHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author ：wust
 * @date ：Created in 2019/8/8 10:04
 * @description：
 * @version:
 */
@Component
public class StrategyContext {

    @Autowired
    private IStrategy adminAccountStrategy;

    @Autowired
    private IStrategy agentAccountStrategy;

    @Autowired
    private IStrategy parentCompanyAccountStrategy;

    @Autowired
    private IStrategy branchCompanyAccountStrategy;

    @Autowired
    private IStrategy projectAccountStrategy;

    @Autowired
    private IStrategy businessAccountStrategy;

    public void bindSql(BaseStatementHandler delegate) {
        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();
        if(ctx.isSuperAdmin()){ // 平台管理员
            adminAccountStrategy.bindSql(delegate);
        }else if(ctx.isAdmin()) { // 平台普通管理员
            adminAccountStrategy.bindSql(delegate);
        }else if(ctx.isStaff()){
            if(DataDictionaryEnum.USER_TYPE_AGENT.getStringValue().equals(ctx.getUser().getType())){ // 代理商
                agentAccountStrategy.bindSql(delegate);
            }else if(DataDictionaryEnum.USER_TYPE_PARENT_COMPANY.getStringValue().equals(ctx.getUser().getType())){ // 总公司
                parentCompanyAccountStrategy.bindSql(delegate);
            }else if(DataDictionaryEnum.USER_TYPE_BRANCH_COMPANY.getStringValue().equals(ctx.getUser().getType())){ // 分公司
                branchCompanyAccountStrategy.bindSql(delegate);
            }else if(DataDictionaryEnum.USER_TYPE_PROJECT.getStringValue().equals(ctx.getUser().getType())){ // 项目
                projectAccountStrategy.bindSql(delegate);
            }else if(DataDictionaryEnum.USER_TYPE_BUSINESS.getStringValue().equals(ctx.getUser().getType())){ // 业务员
                businessAccountStrategy.bindSql(delegate);
            }else{
                throw new BusinessException("非法的SQL请求");
            }
        }else{
            throw new BusinessException("非法的SQL请求");
        }
    }
}
