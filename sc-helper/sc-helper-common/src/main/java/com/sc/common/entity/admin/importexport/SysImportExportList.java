package com.sc.common.entity.admin.importexport;

/**
 * Created by wust on 2019/5/20.
 */
public class SysImportExportList extends SysImportExport {
    private static final long serialVersionUID = -6355731580232808981L;


    private String statusLabel;
    private String operationTypeLabel;



    public String getStatusLabel() {
        return statusLabel;
    }

    public void setStatusLabel(String statusLabel) {
        this.statusLabel = statusLabel;
    }

    public String getOperationTypeLabel() {
        return operationTypeLabel;
    }

    public void setOperationTypeLabel(String operationTypeLabel) {
        this.operationTypeLabel = operationTypeLabel;
    }
}
