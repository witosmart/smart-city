package com.sc.workorder.core.dao;

import com.sc.common.mapper.IBaseMapper;
import com.sc.workorder.entity.WoWorkOrder;

/**
 * @author: wust
 * @date: 2020-04-01 10:27:05
 * @description:
 */
public interface WoWorkOrderMapper extends IBaseMapper<WoWorkOrder>{
}